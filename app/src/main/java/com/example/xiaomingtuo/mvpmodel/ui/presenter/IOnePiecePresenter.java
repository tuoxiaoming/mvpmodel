package com.example.xiaomingtuo.mvpmodel.ui.presenter;

/**
 * Created by xiaoming.tuo on 2017/3/2.
 */

public interface IOnePiecePresenter<T> {

    /**
     * 绑定数据
     */
    void fetch();

    /**
     * 防止内存泄露，添加弱引用
     * @param t view
     */
    void attachView(T t);

    void detachView();
}
