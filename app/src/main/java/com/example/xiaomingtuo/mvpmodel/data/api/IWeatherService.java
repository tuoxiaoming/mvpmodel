package com.example.xiaomingtuo.mvpmodel.data.api;

import com.example.xiaomingtuo.mvpmodel.data.constant.Constant;
import com.example.xiaomingtuo.mvpmodel.data.model.RealtimeWeather;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;
import rx.Observer;

/**
 * Created by xiaoming.tuo on 2017/3/7.
 */

public interface IWeatherService {

    @GET("now/")
    Observable<RealtimeWeather> getRealtimeWeather(@Query("city") String city, @Query("key") String key);

}
