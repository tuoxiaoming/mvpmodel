package com.example.xiaomingtuo.mvpmodel.di.models;

import com.example.xiaomingtuo.mvpmodel.di.scope.ActivityScope;
import com.example.xiaomingtuo.mvpmodel.ui.view.IOnePieceView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by xiaoming.tuo on 2017/3/7.
 */


@ActivityScope

@Module
public final class MainModule {

    private IOnePieceView view;

    public MainModule(IOnePieceView view) {
        this.view = view;
    }

    @Provides
    public IOnePieceView providerView() {
        return this.view;
    }
}
