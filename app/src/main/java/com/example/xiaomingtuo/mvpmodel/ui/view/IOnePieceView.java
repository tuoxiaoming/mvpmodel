package com.example.xiaomingtuo.mvpmodel.ui.view;

import android.widget.BaseAdapter;

import com.example.xiaomingtuo.mvpmodel.data.model.RealtimeWeather;

/**
 * Created by xiaoming.tuo on 2017/3/2.
 */

public interface IOnePieceView {

    /**
     * 显示等待信息
     */
    void showLoading();

    /**
     * 拿到数据后，显示数据
     * @param adapter
     */
    void showData(BaseAdapter adapter);

    void setRealtimeWeather(RealtimeWeather weather);
}
