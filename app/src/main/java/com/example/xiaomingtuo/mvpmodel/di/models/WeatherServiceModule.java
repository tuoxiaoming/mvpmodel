package com.example.xiaomingtuo.mvpmodel.di.models;

import com.example.xiaomingtuo.mvpmodel.data.api.IWeatherService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by xiaoming.tuo on 2017/3/7.
 */

@Module
public class WeatherServiceModule {

    @Singleton
    @Provides
    public IWeatherService provideWeatherService(Retrofit retrofit) {

        return retrofit.create(IWeatherService.class);
    }

}
