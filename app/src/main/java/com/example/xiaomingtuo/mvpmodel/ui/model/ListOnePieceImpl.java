package com.example.xiaomingtuo.mvpmodel.ui.model;

import android.content.Context;
import android.content.res.Resources;

import com.example.xiaomingtuo.mvpmodel.MVPApplication;
import com.example.xiaomingtuo.mvpmodel.R;
import com.example.xiaomingtuo.mvpmodel.ui.bean.OnePiecePerson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by xiaoming.tuo on 2017/3/2.
 */

public class ListOnePieceImpl implements IOnePieceModel {

    private int[] drawables = {R.drawable.data1, R.drawable.data2, R.drawable.data3, R.drawable.data4,
            R.drawable.data5, R.drawable.data6, R.drawable.data7, R.drawable.data8,
            R.drawable.data9, R.drawable.data10, R.drawable.data11, R.drawable.data12};

    @Override
    public void loadData(OnLoadCompleteListener listener) {
        final List<OnePiecePerson> datas = new ArrayList<OnePiecePerson>();
        Context context = MVPApplication.getContext();
        Resources res = context.getResources();
        for (int i = 0; i < drawables.length; i++) {
            OnePiecePerson data = new OnePiecePerson(res.getDrawable(drawables[i]), "data" + i);
            datas.add(data);
        }

        listener.onLoadCompete(datas);
    }
}
