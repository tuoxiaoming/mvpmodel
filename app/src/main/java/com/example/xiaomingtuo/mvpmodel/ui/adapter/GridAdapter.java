package com.example.xiaomingtuo.mvpmodel.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.xiaomingtuo.mvpmodel.R;
import com.example.xiaomingtuo.mvpmodel.ui.bean.OnePiecePerson;

import java.util.List;

/**
 * Created by xiaoming.tuo on 2017/3/2.
 */

public class GridAdapter extends BaseAdapter {
    private Context mContext;
    private List<OnePiecePerson> mData;

    public GridAdapter(Context context, List<OnePiecePerson> data) {
        this.mContext = context;
        this.mData = data;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int i) {
        return mData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        View view;
        ListAdapter.ViewHolder viewHolder;
        if (convertView == null) {
            view = LayoutInflater.from(mContext).inflate(R.layout.layout_list_item, null);
            viewHolder = new ListAdapter.ViewHolder();
            viewHolder.icon = (ImageView) view.findViewById(R.id.icon);
            viewHolder.desc = (TextView) view.findViewById(R.id.desc);
            view.setTag(viewHolder);
        } else {
            view = convertView;
            viewHolder = (ListAdapter.ViewHolder) view.getTag();
        }

        viewHolder.icon.setImageDrawable(mData.get(position).getIcon());
        viewHolder.desc.setText(mData.get(position).getDesc());

        return view;
    }

    static class ViewHolder {
        ImageView icon;
        TextView desc;
    }
}
