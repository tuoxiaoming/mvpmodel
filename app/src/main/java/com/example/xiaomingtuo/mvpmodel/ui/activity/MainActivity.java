package com.example.xiaomingtuo.mvpmodel.ui.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.xiaomingtuo.mvpmodel.R;
import com.example.xiaomingtuo.mvpmodel.ui.fragment.GridFragment;
import com.example.xiaomingtuo.mvpmodel.ui.fragment.ListFragment;

public class MainActivity extends AppCompatActivity {

    FrameLayout view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        view = (FrameLayout) findViewById(R.id.fragment);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        switch (id) {
            case R.id.menu_list:
                transaction.replace(R.id.fragment, new ListFragment());
                break;
            case R.id.menu_grid:
                transaction.replace(R.id.fragment, new GridFragment());
                break;
        }
        transaction.commit();
        return super.onOptionsItemSelected(item);
    }
}
