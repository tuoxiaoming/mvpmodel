package com.example.xiaomingtuo.mvpmodel.ui.presenter;

import android.os.Handler;
import android.widget.BaseAdapter;

import com.example.xiaomingtuo.mvpmodel.data.api.IWeatherService;
import com.example.xiaomingtuo.mvpmodel.data.constant.Constant;
import com.example.xiaomingtuo.mvpmodel.data.model.RealtimeWeather;
import com.example.xiaomingtuo.mvpmodel.ui.adapter.ListAdapter;
import com.example.xiaomingtuo.mvpmodel.ui.bean.OnePiecePerson;
import com.example.xiaomingtuo.mvpmodel.ui.fragment.BaseFragment;
import com.example.xiaomingtuo.mvpmodel.ui.model.IOnePieceModel;
import com.example.xiaomingtuo.mvpmodel.ui.model.ListOnePieceImpl;
import com.example.xiaomingtuo.mvpmodel.ui.view.IOnePieceView;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by xiaoming.tuo on 2017/3/2.
 */

public class ListOnePiecePresenterImpl implements IOnePiecePresenter {

    private IOnePieceView view;
    private IOnePieceModel model;
    private IWeatherService weatherService;

    WeakReference<IOnePieceView> ref ;

    @Inject
    public ListOnePiecePresenterImpl(IOnePieceView view, IWeatherService weatherService) {
        this.view = view;
        this.model = new ListOnePieceImpl();
        this.weatherService = weatherService;
    }

    @Override
    public void fetch() {
        view.showLoading();

        weatherService.getRealtimeWeather("beijing",  Constant.KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Action1<RealtimeWeather>() {
//                    @Override
//                    public void call(RealtimeWeather realtimeWeather) {
//                        view.setRealtimeWeather(realtimeWeather);
//                    }
//                });
                .subscribe(new Subscriber<RealtimeWeather>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e);
                    }

                    @Override
                    public void onNext(RealtimeWeather weather) {
                        view.setRealtimeWeather(weather);
                    }
                });

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                model.loadData(new IOnePieceModel.OnLoadCompleteListener() {
//                    @Override
//                    public void onLoadCompete(List<OnePiecePerson> datas) {
//                        BaseAdapter adapter = new ListAdapter(((BaseFragment) view).getActivity(), datas);
//                        view.showData(adapter);
//                    }
//                });
//            }
//        }, 500);

    }

    @Override
    public void attachView(Object o) {
        ref = new WeakReference<IOnePieceView>((IOnePieceView) o);
    }

    @Override
    public void detachView() {
        ref.clear();
        ref = null;
    }


}
