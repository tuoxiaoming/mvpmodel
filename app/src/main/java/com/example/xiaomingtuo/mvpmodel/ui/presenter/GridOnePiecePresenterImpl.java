package com.example.xiaomingtuo.mvpmodel.ui.presenter;

import android.os.Handler;
import android.widget.BaseAdapter;

import com.example.xiaomingtuo.mvpmodel.ui.adapter.GridAdapter;
import com.example.xiaomingtuo.mvpmodel.ui.bean.OnePiecePerson;
import com.example.xiaomingtuo.mvpmodel.ui.fragment.BaseFragment;
import com.example.xiaomingtuo.mvpmodel.ui.model.GridOnePieceImpl;
import com.example.xiaomingtuo.mvpmodel.ui.model.IOnePieceModel;
import com.example.xiaomingtuo.mvpmodel.ui.view.IOnePieceView;

import java.lang.ref.WeakReference;
import java.util.List;

/**
 * Created by xiaoming.tuo on 2017/3/2.
 */

public class GridOnePiecePresenterImpl implements IOnePiecePresenter {

    IOnePieceView view;
    IOnePieceModel model;
    WeakReference<IOnePieceView> ref;

    public GridOnePiecePresenterImpl(IOnePieceView view) {
        this.view = view;
        this.model = new GridOnePieceImpl();
    }

    @Override
    public void fetch() {
        view.showLoading();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                model.loadData(new IOnePieceModel.OnLoadCompleteListener() {
                    @Override
                    public void onLoadCompete(List<OnePiecePerson> datas) {
                        BaseAdapter adapter = new GridAdapter(((BaseFragment) view).getActivity(), datas);
                        view.showData(adapter);
                    }
                });
            }
        }, 500);

    }

    @Override
    public void attachView(Object o) {
        ref = new WeakReference<IOnePieceView>((IOnePieceView) o);
    }

    @Override
    public void detachView() {
        ref.clear();
        ref = null;
    }


}
