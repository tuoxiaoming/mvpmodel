package com.example.xiaomingtuo.mvpmodel.ui.bean;

import android.os.Parcelable;
import java.util.List;
import com.google.auto.value.AutoValue;
import com.google.gson.TypeAdapter;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import com.google.gson.Gson;

@Generated("com.robohorse.robopojogenerator")
@AutoValue
public abstract class Response implements Parcelable{

	@SerializedName("HeWeather5")
	public abstract List<HeWeather5Item> heWeather5();

	public static TypeAdapter<Response> typeAdapter(Gson gson) {
		return new AutoValue_Response.GsonTypeAdapter(gson);
	}
}