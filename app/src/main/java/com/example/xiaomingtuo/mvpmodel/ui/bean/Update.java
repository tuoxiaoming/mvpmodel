package com.example.xiaomingtuo.mvpmodel.ui.bean;

import android.os.Parcelable;
import com.google.auto.value.AutoValue;
import com.google.gson.TypeAdapter;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import com.google.gson.Gson;

@Generated("com.robohorse.robopojogenerator")
@AutoValue
public abstract class Update implements Parcelable{

	@SerializedName("loc")
	public abstract String loc();

	@SerializedName("utc")
	public abstract String utc();

	public static TypeAdapter<Update> typeAdapter(Gson gson) {
		return new AutoValue_Update.GsonTypeAdapter(gson);
	}
}