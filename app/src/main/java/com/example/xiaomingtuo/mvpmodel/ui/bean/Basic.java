package com.example.xiaomingtuo.mvpmodel.ui.bean;

import com.google.auto.value.AutoValue;
import com.google.gson.TypeAdapter;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import com.google.gson.Gson;

@Generated("com.robohorse.robopojogenerator")
@AutoValue
public abstract class Basic{

	@SerializedName("city")
	public abstract String city();

	@SerializedName("update")
	public abstract Update update();

	@SerializedName("lon")
	public abstract String lon();

	@SerializedName("id")
	public abstract String id();

	@SerializedName("prov")
	public abstract String prov();

	@SerializedName("cnty")
	public abstract String cnty();

	@SerializedName("lat")
	public abstract String lat();

	public static TypeAdapter<Basic> typeAdapter(Gson gson) {
		return new AutoValue_Basic.GsonTypeAdapter(gson);
	}
}