package com.example.xiaomingtuo.mvpmodel.di.component;

import com.example.xiaomingtuo.mvpmodel.di.models.AppModule;
import com.example.xiaomingtuo.mvpmodel.di.models.MainModule;
import com.example.xiaomingtuo.mvpmodel.di.models.NetWorkModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by xiaoming.tuo on 2017/3/7.
 */

@Singleton
@Component(modules = {AppModule.class, NetWorkModule.class})
public interface APPComponent {

    MainComponent addSub(MainModule mainModule) ;

}
