package com.example.xiaomingtuo.mvpmodel.ui.bean;

import java.util.List;
import com.google.auto.value.AutoValue;
import com.google.gson.TypeAdapter;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import com.google.gson.Gson;

@Generated("com.robohorse.robopojogenerator")
@AutoValue
public abstract class HeWeather5Item{

	@SerializedName("alarms")
	public abstract List<AlarmsItem> alarms();

	@SerializedName("basic")
	public abstract Basic basic();

	@SerializedName("status")
	public abstract String status();

	public static TypeAdapter<HeWeather5Item> typeAdapter(Gson gson) {
		return new AutoValue_HeWeather5Item.GsonTypeAdapter(gson);
	}
}