package com.example.xiaomingtuo.mvpmodel.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.Toast;

import com.example.xiaomingtuo.mvpmodel.R;
import com.example.xiaomingtuo.mvpmodel.data.model.RealtimeWeather;
import com.example.xiaomingtuo.mvpmodel.ui.presenter.GridOnePiecePresenterImpl;
import com.example.xiaomingtuo.mvpmodel.ui.view.IOnePieceView;

/**
 * Created by xiaoming.tuo on 2017/3/2.
 */

public class GridFragment extends BaseFragment implements IOnePieceView {

    private GridView gridView;
    private GridOnePiecePresenterImpl presenter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_grid, null);
        gridView = (GridView) view.findViewById(R.id.grid);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter = new GridOnePiecePresenterImpl(this);
        presenter.attachView(this);
        presenter.fetch();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();

    }

    @Override
    public void showLoading() {
        Toast.makeText(getActivity(), "onLoading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showData(BaseAdapter adapter) {
        gridView.setAdapter(adapter);
    }

    @Override
    public void setRealtimeWeather(RealtimeWeather weather) {

    }
}
