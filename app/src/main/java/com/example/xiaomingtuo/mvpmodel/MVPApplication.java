package com.example.xiaomingtuo.mvpmodel;

import android.app.Application;
import android.content.Context;

import com.example.xiaomingtuo.mvpmodel.di.component.APPComponent;
import com.example.xiaomingtuo.mvpmodel.di.component.DaggerAPPComponent;
import com.example.xiaomingtuo.mvpmodel.di.models.AppModule;

/**
 * Created by xiaoming.tuo on 2017/3/2.
 */

public class MVPApplication extends Application {

    private static Context sContext;
    private static APPComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;

        //此时appModule方法是过时方法，因为我们没有使用到任何一个module中提供的对象

        appComponent = DaggerAPPComponent.builder().appModule(new AppModule(this)).build();
    }

    public static MVPApplication get(Context context){
        return (MVPApplication) context.getApplicationContext();
    }

    public static Context getContext(){
        return sContext;
    }

    public  APPComponent getAppComponent() {
        return appComponent;
    }
}
