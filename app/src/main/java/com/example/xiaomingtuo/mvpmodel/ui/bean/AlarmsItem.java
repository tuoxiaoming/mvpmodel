package com.example.xiaomingtuo.mvpmodel.ui.bean;

import com.google.auto.value.AutoValue;
import com.google.gson.TypeAdapter;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;
import com.google.gson.Gson;

@Generated("com.robohorse.robopojogenerator")
@AutoValue
public abstract class AlarmsItem{

	@SerializedName("txt")
	public abstract String txt();

	@SerializedName("stat")
	public abstract String stat();

	@SerializedName("level")
	public abstract String level();

	@SerializedName("title")
	public abstract String title();

	@SerializedName("type")
	public abstract String type();

	public static TypeAdapter<AlarmsItem> typeAdapter(Gson gson) {
		return new AutoValue_AlarmsItem.GsonTypeAdapter(gson);
	}
}