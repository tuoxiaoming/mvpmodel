package com.example.xiaomingtuo.mvpmodel.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.xiaomingtuo.mvpmodel.MVPApplication;
import com.example.xiaomingtuo.mvpmodel.R;
import com.example.xiaomingtuo.mvpmodel.data.model.RealtimeWeather;
import com.example.xiaomingtuo.mvpmodel.di.models.MainModule;
import com.example.xiaomingtuo.mvpmodel.ui.presenter.ListOnePiecePresenterImpl;
import com.example.xiaomingtuo.mvpmodel.ui.view.IOnePieceView;

import javax.inject.Inject;

/**
 * Created by xiaoming.tuo on 2017/3/2.
 */

public class ListFragment extends BaseFragment implements IOnePieceView {

    private ListView listView;
    private TextView tv;
    @Inject
    ListOnePiecePresenterImpl presenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MVPApplication.get(getActivity()).getAppComponent().addSub(new MainModule(this)).inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, null);
        listView = (ListView) view.findViewById(R.id.list);
        tv = (TextView) view.findViewById(R.id.weather);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        presenter = new ListOnePiecePresenterImpl(this);

        presenter.attachView(this);
        presenter.fetch();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showLoading() {
        Toast.makeText(getActivity(), "onLoading", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showData(BaseAdapter adapter) {

        listView.setAdapter(adapter);
    }

    @Override
    public void setRealtimeWeather(RealtimeWeather weather) {
        tv.setText("the temperature now is:" + weather.getHeWeather5().get(0).getNow().getTmp() + " °C");
    }
}
