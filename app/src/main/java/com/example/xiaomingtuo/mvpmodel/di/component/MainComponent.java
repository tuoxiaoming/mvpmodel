package com.example.xiaomingtuo.mvpmodel.di.component;

import com.example.xiaomingtuo.mvpmodel.di.models.MainModule;
import com.example.xiaomingtuo.mvpmodel.di.scope.ActivityScope;
import com.example.xiaomingtuo.mvpmodel.ui.fragment.ListFragment;

import dagger.Subcomponent;

/**
 * Created by xiaoming.tuo on 2017/3/7.
 */


//很重要！这个Component应该是AppComponent的子Component，所以要使用这个注解
//不使用@Component注解的Dependents属性是因为希望能统一管理子Component
@Subcomponent(modules = MainModule.class)
//生命周期管理
@ActivityScope

public interface MainComponent {

    //方法参数中，只能传递被注入对象！要在哪个类中注入，写哪个类，注入到父类没用！
    void inject(ListFragment ListFragment) ;
}
