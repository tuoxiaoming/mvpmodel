package com.example.xiaomingtuo.mvpmodel.ui.model;

import com.example.xiaomingtuo.mvpmodel.ui.bean.OnePiecePerson;

import java.util.List;

/**
 * Created by xiaoming.tuo on 2017/3/2.
 */

public interface IOnePieceModel {

    /**
     * 加载数据
     * @param listener
     */
    void loadData(OnLoadCompleteListener listener);

    /**
     * 加载完数据回调
     */
    interface OnLoadCompleteListener {
        void onLoadCompete(List<OnePiecePerson> data);
    }
}
