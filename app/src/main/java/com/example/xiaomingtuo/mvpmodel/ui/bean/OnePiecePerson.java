package com.example.xiaomingtuo.mvpmodel.ui.bean;

import android.graphics.drawable.Drawable;

/**
 * Created by xiaoming.tuo on 2017/3/2.
 */

public final class OnePiecePerson {

    public OnePiecePerson(Drawable icon, String desc) {
        this.icon = icon;
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    private String desc;

    private Drawable icon;
    public Drawable getIcon() {
        return icon;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }


}
