package com.example.xiaomingtuo.mvpmodel.di.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.example.xiaomingtuo.mvpmodel.MVPApplication;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import timber.log.Timber;

/**
 * Created by xiaoming.tuo on 2017/3/7.
 */

@Module
public final class AppModule {

    private MVPApplication application;

    public AppModule(MVPApplication application) {
        this.application = application;
    }

    @Singleton
    @Provides
    public Context providerApplicationContext(){
        return application.getApplicationContext();
    }

    @Singleton
    @Provides
    public SharedPreferences providerSharePresferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }


    @Singleton
    @Provides
    public Picasso providerPicasso(Context context) {
        return new Picasso.Builder(context)
                .listener(new Picasso.Listener() {
                    @Override
                    public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                        Timber.e(exception,"failed to load image: %s", uri);
                    }
                })
                .build();
    }
}
